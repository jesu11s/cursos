from structures.binary_search_tree import BinarySearchTree

import pytest



@pytest.fixture
def tree_data():
    # Tree form
    #                          30
    #            15                         45
    #       10           22            39          65
    #    8     12      18   26      33    42    52    90
    #   4  9  11 13  16 19 23 27  31 35 41 44  47 55 88 91

    my_tree = BinarySearchTree()
    my_tree.insert(30)
    my_tree.insert(45)
    my_tree.insert(15)
    my_tree.insert(10)
    my_tree.insert(65)
    my_tree.insert(22)
    my_tree.insert(39)

    my_tree.insert(8)
    my_tree.insert(12)
    my_tree.insert(18)
    my_tree.insert(26)
    my_tree.insert(33)
    my_tree.insert(42)
    my_tree.insert(52)
    my_tree.insert(90)

    my_tree.insert(4)
    my_tree.insert(9)
    my_tree.insert(11)
    my_tree.insert(13)
    my_tree.insert(16)
    my_tree.insert(19)
    my_tree.insert(23)
    my_tree.insert(27)

    my_tree.insert(31)
    my_tree.insert(35)
    my_tree.insert(41)
    my_tree.insert(44)
    my_tree.insert(47)
    my_tree.insert(55)
    my_tree.insert(88)
    my_tree.insert(91)

    return my_tree

def test_b_f_s_loop(tree_data):
    expected_result = [
                        30, 
                        15, 
                        45, 
                        10, 
                        22, 
                        39, 
                        65, 
                        8, 
                        12, 
                        18, 
                        26, 
                        33, 
                        42, 
                        52, 
                        90, 
                        4, 
                        9, 
                        11, 
                        13, 
                        16, 
                        19, 
                        23, 
                        27, 
                        31, 
                        35, 
                        41, 
                        44, 
                        47, 
                        55, 
                        88, 
                        91,
                        ]

    result = tree_data.breadth_first_search()
    print(f'Result :{result}')
    assert result == expected_result
    

def test_b_f_s_recursive(tree_data):
    expected_result = [
                    30, 
                    15, 
                    45, 
                    10, 
                    22, 
                    39, 
                    65, 
                    8, 
                    12, 
                    18, 
                    26, 
                    33, 
                    42, 
                    52, 
                    90, 
                    4, 
                    9, 
                    11, 
                    13, 
                    16, 
                    19, 
                    23, 
                    27, 
                    31, 
                    35, 
                    41, 
                    44, 
                    47, 
                    55, 
                    88, 
                    91,
                    ]
    result = tree_data.breadth_first_search_rec([tree_data.root],
                                          [])
    assert result == expected_result


def test_d_f_s(tree_data):
    expected_result = [
                    30, 
                    10,  
                    8,
                    4, 
                    9, 
                    12,
                    11, 
                    13, 
                    22, 
                    18, 
                    16, 
                    19, 
                    26, 
                    23, 
                    27, 
                    45, 
                    39, 
                    33, 
                    31, 
                    35, 
                    42, 
                    41, 
                    44, 
                    65, 
                    52, 
                    47, 
                    55, 
                    90, 
                    88, 
                    91]

    tree_data.remove_rearrange(15)
    result = tree_data.DFS_preorder()
    print(f'DFS_preorder: {result}')
    assert result == expected_result

def test_empty_tree():

    my_tree = BinarySearchTree()
    result = my_tree.remove_rearrange(15)
    expected_result  = None
    assert result == expected_result


def test_remove_root(tree_data):
    expected_result = [ 
                    15,
                    10,  
                    8,
                    4, 
                    9, 
                    12,
                    11, 
                    13, 
                    22, 
                    18, 
                    16, 
                    19, 
                    26, 
                    23, 
                    27, 
                    45, 
                    39, 
                    33, 
                    31, 
                    35, 
                    42, 
                    41, 
                    44, 
                    65, 
                    52, 
                    47, 
                    55, 
                    90, 
                    88, 
                    91]

    tree_data.remove_rearrange(30)
    result = tree_data.DFS_preorder()
    print(f'DFS_preorder: {result}')
    assert result == expected_result

@pytest.mark.parametrize(["inputs","expected_results"],[
    (11,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        # 11, 
        13, 
        22, 
        18, 
        16, 
        19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        91
        ]),
    (13,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        # 13, 
        22, 
        18, 
        16, 
        19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        91
        ]),
    (16,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        13, 
        22, 
        18, 
        # 16, 
        19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        91
        ]),
    (19,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        13, 
        22, 
        18, 
        16, 
        # 19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        91
        ]),
    (23,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        13, 
        22, 
        18, 
        16, 
        19, 
        26, 
        # 23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        91
        ]),
    (88,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        13, 
        22, 
        18, 
        16, 
        19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        # 88,
        91
        ]),
    (91,[ 
        30,
        15,
        10,  
        8,
        4, 
        9, 
        12,
        11, 
        13, 
        22, 
        18, 
        16, 
        19, 
        26, 
        23, 
        27, 
        45, 
        39, 
        33, 
        31, 
        35, 
        42, 
        41, 
        44, 
        65, 
        52, 
        47, 
        55, 
        90, 
        88,
        # 91
        ]),
])
def test_remove_leafs(tree_data,inputs,expected_results):

    tree_data.remove_rearrange(inputs)
    result = tree_data.DFS_preorder()
    print(f'DFS_preorder: {result}')
    assert result == expected_results

# Tree form
#                          30
#            15                         45
#       10           22            39          65
#    8     12      18   26      33    42    52    90
#  4   9  11 13  16 19 23 27  31 35 41 44  47 55 88 91
