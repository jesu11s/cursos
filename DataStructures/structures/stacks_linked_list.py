class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class Stacks:
    def __init__(self):
        self.top = None
        self.bottom = None
        self.length = 0

    def peek(self):
        return self.top.value
        
    def push(self, value):
        new_top = Node(value)
        if not self.top:
            self.top = new_top
            self.bottom = new_top
        else:
            new_top.next = self.top
            self.top = new_top
            
        self.length += 1
        
    def pop(self):
        if not self.top:
            return None
        if self.bottom == self.top:
            self.bottom = None
        value_to_return = self.top.value
        self.top = self.top.next
        self.length -= 1
        return value_to_return

if __name__ == "__main__":
    my_stack = Stacks()
    my_stack.push(1)
    my_stack.push(3)
    my_stack.push(5)

    obtenido = my_stack.pop()
    print(f"El pop1 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop2 {obtenido}")
    obtenido = my_stack.peek()
    print(f"El peek {obtenido}")
    
    obtenido = my_stack.pop()
    print(f"El pop3 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop4 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop5 {obtenido}")