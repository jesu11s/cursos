"""
Ejemplo de linked list:
    {value: 1,
    next: {
        value: 2,
        next: {
            value:5,
            next: None
        }
    }
}
"""
class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
        self.before = None
    # def __str__(self):
    #     return f"Value {self.value}, next:{self.next}, before: {self.before}"

class LinkedList:

    def __init__(self,value):
        self.head = Node(value)
        self.tail = self.head
        self.length = 1
    
    def append(self,value):
        new_tail = Node(value)
        new_tail.before = self.tail
        self.tail.next = new_tail
        self.tail = new_tail
        self.length += 1
    
    def preppend(self,value):
        new_tail = Node(value)
        self.head.before = new_tail
        new_tail.next = self.head
        self.head = new_tail
        self.length += 1

    def insert(self,index,value):
        if index == 0:
            self.preppend(value)
            return
        new_node = Node(value)
        new_head = self.head
        
        print("Comienza  a buscar")
        for i in range(self.length):
            print(f"index actual = {i}")
            print(f"new_head.value actual = {new_head.value}")
            if i == index-1:                
                break
            print("Se salto el break")
            new_head = new_head.next    
            
        new_tail = new_head.next
        new_head.next = new_node
        new_node.before = new_head
        new_node.next = new_tail
        new_tail.before = new_node

        new_head.next = new_node
        self.length += 1

    def remove(self,index):
        new_head = self.head
        for i in range(self.length):
            if i == index - 1:
                break
            new_head = new_head.next
        unwanted_node = new_head.next
        new_tail = unwanted_node.next
        new_tail.before = new_head
        new_head.next = new_tail
        self.length -= 1

    def __str__(self):
        elements = []
        nodes = self.head
        for i in range(self.length):
            
            elements.append(nodes.value)
            nodes = nodes.next
        return elements.__str__()

        
new_linked_list = LinkedList(3)
new_linked_list.append(5)
new_linked_list.append(10)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")

new_linked_list.preppend(33)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
new_linked_list.insert(2,122)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")

new_linked_list.insert(2,44)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")

new_linked_list.remove(3)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
new_linked_list.remove(1)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")