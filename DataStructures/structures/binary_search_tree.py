from structures.node import Node
            
class BinarySearchTree:
    def __init__(self):
        self.root = None

    def insert(self,value):
        new_node = Node(value)
        if not self.root:
            self.root = new_node
            return
        actual_root = self.root
        while True:
            if actual_root.value == value:
                print(f"Value has been inserted already: {value}")
                return
            if actual_root.value > value:
                if not actual_root.left:
                    actual_root.left = new_node
                    break
            
                actual_root = actual_root.left
            
            else:
                if not actual_root.rigth:
                    actual_root.rigth = new_node
                    break
                actual_root = actual_root.rigth
            
    
    def lookup(self,value):
        current_node = self.root
        if current_node.value == value:
            return f"Found value: {value} in tree"
        while True:
            # print(f"Current node: {current_node}")
            if current_node == None:
                return None
            if current_node.value == value:
                return current_node
            if current_node.left and current_node.left == value:
                return current_node.left
            elif current_node.rigth and current_node.rigth == value:
                return current_node.rigth
            
            if current_node.value < value:
                current_node = current_node.rigth
                # print(f'Se va por la derecha: {current_node}')
            else:
                current_node = current_node.left
                # print(f'Se va por la izquierda: {current_node}')

    def breadth_first_search(self):
        elements = []
        queue = []
        queue.append(self.root)
        while queue.__len__() > 0:
            current_node = queue.pop(0)
            elements.append(current_node.value)
            if current_node.left:
                queue.append(current_node.left)
            if current_node.rigth:
                queue.append(current_node.rigth)
        return elements

    def breadth_first_search_rec(self,
                                 queue,
                                 elements):
        if queue.__len__() == 0:
            return elements
        
        current_node = queue.pop(0)
        elements.append(current_node.value)
        if current_node.left:
            queue.append(current_node.left)

        if current_node.rigth:
            queue.append(current_node.rigth)
        result = self.breadth_first_search_rec(queue,elements)
        return result

    def DFS_inorder(self):
        return self.traverse_in_order(self.root,[])

    def DFS_preorder(self):
        return self.traverse_preorder(self.root,[])

    def DFS_postorder(self):
        return self.traverse_postorder(self.root,[])
        
    def traverse_postorder(self,node,elements):        
        if node.left:
            self.traverse_postorder(node.left,elements)
        
        if node.rigth:
            self.traverse_postorder(node.rigth,elements)      
        elements.append(node.value)
        return elements

    def traverse_preorder(self,node,elements):
        if node.value:
            elements.append(node.value)
        if node.left:
            self.traverse_preorder(node.left,elements)
        if node.rigth:
            self.traverse_preorder(node.rigth,elements)
        return elements

    def traverse_in_order(self,node,elements):
        if node.left:
            self.traverse_in_order(node.left,elements)
        elements.append(node.value)
        if node.rigth:
            self.traverse_in_order(node.rigth,elements)
        return elements

    def remove(self,value):
        current_node = self.root
        found_node = self.lookup(value)
        if found_node == None:
            return None
        while True:
            new_value =found_node.left
            # if theres not more nodes
            if new_value == None:
                # remove the value to avoid repetition
                found_node.value = None
                return 
            found_node.value = new_value.value
            found_node = new_value

    def remove_rearrange(self, value):
        current_node = self.root
        print(f"Vamo a elminar el {value}*******************")
        if self.root is None:
            print('Tree is empty')
            return None
        # Root is the desired value
        if current_node.value == value:
            print(f"Found value: {value} in root")
        else:
            while True:
                if current_node == None:
                    break
                if current_node.value == value:
                    # pending_children = current_node
                    break
                if current_node.left and current_node.left == value:
                    # pending_children = current_node.left
                    break
                elif current_node.rigth and current_node.rigth == value:
                    # pending_children = current_node.rigth
                    break
                
                if current_node.value < value:
                    current_node = current_node.rigth
                    # print(f'Se va por la derecha: {current_node}')
                else:
                    current_node = current_node.left

        # Verify leafs
        # if (pending_children.left is None and
        #     pending_children.rigth is None):
        #     del current_node
        #     print('Se borro el nodo')
        #     return
        while current_node.value != None:
            print(f'current_node.value: {current_node.value}') 
            if current_node.left:
                print(f'current_node.left: {current_node.left.value}') 
            if current_node.left:
                print("Reasigno el nodo izquierdo")
                print(f'Con: {current_node.left.value}') 
                # assign left child value to node wanted to remove 
                current_node.value = current_node.left.value
                # iterate over the next left node
                current_node = current_node.left
            elif current_node.rigth:
                print("Reasigno el nodo derecho")
                current_node.value = pending_children.rigth.value
                current_node = current_node.rigth
            
            else:
                print('Here')
  
                print(f'current_node.value: {current_node.value}') 
                current_node.value = None
                print('d')
                
                print('Value found!')     
                
 
        
