from time import time
from queue import Queue
from datetime import datetime
from datetime import timedelta
class Runner:
    def __init__(self,time):
        self.time = time
        self.queue = Queue(10)
    def runner(self):
        start = datetime.now()
        ends = start + timedelta(seconds=self.time)
        contador = 0
        valores = [50,100,500]
        while ends > datetime.now():
            contador += 1
            if contador in valores:
                self.queue.put(contador)
    
    
    