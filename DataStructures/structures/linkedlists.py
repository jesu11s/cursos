"""
Ejemplo de linked list:
    {value: 1,
    next: {
        value: 2,
        next: {
            value:5,
            next: None
        }
    }
}
"""
class Node:
    def __init__(self, value):
        self.value = value
        self.next = None
    # def __str__(self):
    #     return f"Value {self.value}, next:{self.next}"

class LinkedList:

    def __init__(self,value):
        self.head = Node(value)
        self.tail = self.head
        self.length = 1
    
    def append(self,value):
        new_tail = Node(value)
        self.tail.next = new_tail
        self.tail = new_tail
        self.length += 1
    
    def preppend(self,value):
        new_tail = Node(value)
        new_tail.next = self.head
        self.head = new_tail
        self.length += 1

    def insert(self,index,value):
        if index == 0:
            self.preppend(value)
            return
        new_node = Node(value)
        new_head = self.head
        
        print("Comienza  a buscar")
        for i in range(self.length):
            print(f"index actual = {i}")
            print(f"new_head.value actual = {new_head.value}")
            if i == index-1:                
                break
            print("Se salto el break")
            new_head = new_head.next    
            
        new_tail = new_head.next
        new_head.next = new_node
        new_node.next = new_tail

        new_head.next = new_node
        self.length += 1
    def remove(self,index):
        new_head = self.head
        for i in range(self.length):
            if i == index - 1:
                break
            new_head = new_head.next
        unwanted_node = new_head.next
        new_head.next = unwanted_node.next
        self.length -= 1
    def __str__(self):
        elements = []
        nodes = self.head
        for i in range(self.length):
            
            elements.append(nodes.value)
            nodes = nodes.next
        return elements.__str__()

    def reverse(self):
        if not self.head.next:
            return self.head
        new_tail = Node(self.head.value)
        new_head = new_tail
        actual_node = self.head
        
        for i in range(self.length):      
            
            if actual_node.next == None:
                break
            
            actual_node = actual_node.next
            # It makes what preppend operates
            new_node_to_preppend = Node(actual_node.value)
            new_node_to_preppend.next = new_head
            new_head = new_node_to_preppend

        self.head = new_head
        self.tail = new_tail
        return new_head
            
    def reverse_v2(self):
        if not self.head.next:
            return self.head
        first = self.head
        self.tail = self.head
        second = first.next
        while second:
            temp = second.next
            second.next = first
            first = second
            second = temp
        print(f"antes de terminar {self.__str__()}")
        print(f"antes de terminar el next:{self.head.next.value}")
        
        print(f"antes de terminar el next:{self.head.next}")
        self.head = first
        print(f"antes de terminar el head es:{self.__str__()}")


        



new_linked_list = LinkedList(3)
new_linked_list.append(5)
new_linked_list.append(10)
new_linked_list.append(11)
new_linked_list.append(12)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
new_linked_list.preppend(100)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
new_linked_list.insert(1,22)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
new_linked_list.insert(0,33)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")

new_linked_list.remove(5)
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")
print("se hizo el reverse")
new_linked_list.reverse_v2()
print(f"head: {new_linked_list}")
print(f"length: {new_linked_list.length}")