class Node:
    def __init__(self, value):
        self.value = value
        self.next = None

class Queues:
    def __init__(self):
        self.top = None
        self.bottom = None
        self.length = 0

    def peek(self):
        if not self.top:
            return None
        return self.top.value
        
    def enqueue(self, value):
        new_bottom = Node(value)
        if not self.top:
            print("if")
            self.top = new_bottom
        else:
            print("else")
            self.bottom.next = new_bottom
        self.bottom = new_bottom
            
        self.length += 1
        
    def dequeue(self):
        if not self.top:
            print("Aqui")
            return None
        if self.bottom == self.top:
            self.bottom = None
            
        value_to_return = self.top.value
        self.top = self.top.next
        self.length -= 1
        return value_to_return

if __name__ == "__main__":
    my_queue = Queues()
    my_queue.enqueue(1)
    my_queue.enqueue(3)
    my_queue.enqueue(5)
    my_queue.enqueue(7)

    obtenido = my_queue.dequeue()
    print(f"El dequeue {obtenido}")
    obtenido = my_queue.dequeue()
    print(f"El dequeue {obtenido}")
    obtenido = my_queue.peek()
    print(f"El dequeue {obtenido}")
    
    obtenido = my_queue.dequeue()
    print(f"El dequeue {obtenido}")
    obtenido = my_queue.dequeue()
    print(f"El dequeue {obtenido}")
    obtenido = my_queue.dequeue()
    print(f"El dequeue {obtenido}")