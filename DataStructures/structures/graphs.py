class Graph:
    def __init__(self):
        self.numberOfNodes = 0
        self.adjacentList = {}
    
    def addVertex(self,node):
        if self.adjacentList.get(node):
            raise Exception(f"This node has already created: {node}")
        self.adjacentList[node] = []
        self.numberOfNodes += 1
        
    def addEdge(self,node1, node2):
        node1_content = self.adjacentList.get(node1,None)
        node2_content = self.adjacentList.get(node2,None)
        print(f"node: {node1_content}")
        if not node1_content is None:
            if node2 in node1_content or node1 in node2_content:
                raise Exception(f"The node: {node1} "  + 
                f"has vertex to node:{node2}")
            else:
                self.adjacentList[node1].append(node2)
                self.adjacentList[node2].append(node1)
        else:
            raise Exception(f"The node: {node1} has not been created")
    
    def showConnections(self):
        for k in self.adjacentList.keys():
            print(f"{k} --> {self.adjacentList.get(k)}")
        

my_graph = Graph()
my_graph.addVertex('0')
my_graph.addVertex('1')
my_graph.addVertex('2')
my_graph.addVertex('3')
my_graph.addVertex('4')

my_graph.addEdge('0','1')
my_graph.addEdge('0','2')
my_graph.addEdge('1','3')
my_graph.addEdge('2','3')
my_graph.addEdge('3','4')
# my_graph.addEdge('3','1')
my_graph.showConnections()