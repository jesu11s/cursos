
class Stacks:
    def __init__(self):
        self.top = []
        self.length = 0
    def peek(self):
        return self.top[self.length-1]
        
    def push(self, value):
        if not self.top:
            self.top = [value]
        else:
            self.top.append(value)
        self.length += 1
        
    def pop(self):
        if not self.top:
            return None
        value_to_return = self.top.pop()

        self.length -= 1
        return value_to_return

        

if __name__ == "__main__":
    my_stack = Stacks()
    my_stack.push(1)
    my_stack.push(3)
    my_stack.push(5)
    my_stack.push(7)
    obtenido = my_stack.peek()
    print(f"El peek {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop1 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop2 {obtenido}")

    
    obtenido = my_stack.pop()
    print(f"El pop3 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop4 {obtenido}")
    obtenido = my_stack.pop()
    print(f"El pop5 {obtenido}")