from string import punctuation

from structures import graphs

with open('resources/quijote.txt') as file:
    quijote_data = file.read()

print(quijote_data)
# Remove commas, period and so on
punctuation += '¡'+'¿'
for punc in punctuation:
    quijote_data = quijote_data.replace(punc, '')
quijote_data = quijote_data.split()
print(quijote_data)

my_graph = graphs.Graphs()

