
array = [2,41,12,32,67,78,2,39,20]

def tortoise_hare_search(array):
    tortoise_position = 0
    hare_position = 1
    tortoise = array[tortoise_position]
    hare = array[hare_position]
    print(f'Esta es la tortoise: {tortoise}')
    print(f'Esta es la hare: {hare}')
    if tortoise == hare:
        return tortoise
    print('Entra al for')
    while True:
        tortoise_position += 1
        hare_position += 2
        difference = hare_position - array.__len__()
        if difference >= 0:
            hare_position = difference
        if tortoise_position >= array.__len__()-1:
            break
        tortoise = array[tortoise_position]
        hare = array[hare_position]
        print(f'Esta es la tortoise: {tortoise}')
        print(f'Esta es la hare: {hare}')
        if tortoise == hare:
            return tortoise
    return None
result = tortoise_hare_search(array)
print(f'numero repetido es: {result}')
