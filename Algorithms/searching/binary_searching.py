from algorithms.sorting.merge_sorting import merge_sorting

array = [1,4,31,234,14,25,67,8,79,43,12,3,54]
elemento_buscado =  26

def binary_search(array, element):
    """
    Params:
        array: arreglo con elementos a buscar
        element: elemento a buscar en arreglo
    Returns:
        int: Posicion del elemento buscado
    """
    largo_arreglo = array.__len__()
    # print(f"Largo arreglo actual : {largo_arreglo}")
    # si el arreglo es de un solo elemento, terminar busqueda
    if largo_arreglo == 1:
        return None
    mitad = largo_arreglo // 2
    inicio_subarreglo = 0
    final_subarreglo = mitad
    if array[mitad] == element:
        return mitad
    elif array[mitad] < element:
        inicio_subarreglo = mitad
        final_subarreglo = largo_arreglo
        subarray = array[inicio_subarreglo:final_subarreglo]
        binary_search(subarray,element)
    elif array[mitad] > element:
        subarray = array[inicio_subarreglo:final_subarreglo]
        binary_search(subarray,element)
    else:
        return None
    
sorted_array = merge_sorting(array)
print(f'Arreglo original: {array}')
print(f'Arreglo ordenado: {sorted_array}')
print(f'Elemento a buscar: {elemento_buscado}')
print('--------Inicio de busqueda binaria-------')
posicion_encontrada = binary_search(sorted_array,elemento_buscado)
print(f'Posición de elemento buscado: {posicion_encontrada}')