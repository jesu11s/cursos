

def reverse_it(word):
    new_word = ""
    for pos in range(len(word)-1,-1,-1):
        letra = word[pos]
        new_word += letra

    return new_word
palabra = "pedrito"  
response = reverse_it(palabra)
print(palabra)
print(response)

def reverse_recursion(word):
    print(f"word is: {word}")
    if word.__len__() < 2:
        return word
    print(f"word is: {word}")
    new_word = word[1:]
    reversed_word = reverse_recursion(new_word)+ word[0]
    return reversed_word

palabra = "pedrito"  
response = reverse_recursion(palabra)
print(palabra)
print(response)