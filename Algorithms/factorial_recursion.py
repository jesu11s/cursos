def factorial_recursive(number):
    # base cade
    if number <= 1:
        return number
    # recursive case
    return number * factorial_recursive(number -1)

def factorial_loop(number):
    result = 1
    for i in range(1,number+1):
        result = result * i
    return result

print(factorial_recursive(6))
print(factorial_loop(6))