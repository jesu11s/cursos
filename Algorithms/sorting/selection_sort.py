array = [4,54,23,1,90,12,3,4]

def selection_sort(array):
    len_array = 0
    global_min = array[0]
    global_min_pos = 0
    for len_array in range(array.__len__()-1):
        for pos in range(len_array+1,array.__len__()):
            if global_min > array[pos]:
                global_min = array[pos]
                global_min_pos = pos
        # Se guarda el valor actual de posicion actual
        temp_min_value = array[len_array]
        # se asigna el minimo global a la primer posicion
        array[len_array] = global_min
        # el valor almacenado se asigna a 
        # donde se encontraba el minimo global
        array[global_min_pos] = temp_min_value
        # el minimo global temporal ahora será la siguiente posicion
        global_min = array[len_array+1]
        # y se asigna la siguiente posicon del minimo global
        global_min_pos = len_array+1
        
        print(f"El array actual: {array}")
    return array

result = selection_sort(array)
print(result)