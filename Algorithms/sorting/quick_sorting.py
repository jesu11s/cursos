numbers = [65,4,22,1,4,3,32,56,3,22,12,43,15]

def reorder_array(array,
                  position_element,
                  position_pivot):
    # Rspect elements from the begining
    new_array = array[:position_element]
    # add the new elements from the next element
    # to move to the rigth of current pivot
    new_array += array[position_element+1:position_pivot]
    # Add current pivot
    new_array.append(array[position_pivot])
    # Add element next to pivot
    new_array.append(array[position_element])
    # add the rest of the array
    new_array += array[position_pivot+1:]
    print(f'new_array: {new_array}')
    return new_array
    

def quick_sorting(array):
    print(f'Nueva iteracion: current array: {array}')
    pivot_position = array.__len__()-1
    pivot = array[pivot_position]
    if array.__len__() == 1:
        return array
    pos = 0
    print(f'Current pivot: {pivot}')
    print(f'Current pivot_position: {pivot_position}')
    while True:
        # verify that current position its not the same
        # as the current pivot position
        # verify by position instead of value in order to avoid
        # errors when array has two equals values
        if pos == pivot_position : 
            print(f'Current pos: {pos}')
            break
        if array[pos]>pivot:
            array = reorder_array(array,pos,pivot_position)
            pivot_position -= 1
        else:
            pos += 1
    print(f'Reorganizado: {array}')
    left_array = array[:pivot_position]
    rigth_array = array[pivot_position:]
    arranged_array_left = quick_sorting(left_array)
    arranged_array_rigth = quick_sorting(rigth_array)
    array = arranged_array_left + arranged_array_rigth
    return array

result = quick_sorting(numbers)
print(f'result : {result}')