import random
numbers = [3,2,5,1,9,10,12,32,39,86,200,6,19,78,45,99]
# 3,2,5,1                           12,32,6,39                       
# 3,2    5,1                        12,32     39,6
# 3   2    5   1                    12   32    39   6
# 2,3    1,5                        12,32      6,39
# 1,2,3,5                           6,12,32,39


# TODO implementar de manera iterativa
def merge_sorting(numbers):
    # split numbers on half
    if numbers.__len__() == 1:
        return numbers
    half_length = numbers.__len__() // 2
    left_numbers = numbers[0:half_length]
    right_numbers = numbers[half_length:]
    left_numbers =  merge_sorting(left_numbers)
    left_len = left_numbers.__len__()
    right_numbers = merge_sorting(right_numbers)
    rigth_len = right_numbers.__len__()
    print(f'left numbers: {left_numbers}')
    print(f'left numbers len: {left_numbers.__len__()}')
    print(f'rigth numbers: {right_numbers}')
    print(f'rigth numbers len: {right_numbers.__len__()}')
    new_numbers = []
    # Se utilizan dos contadores por que se desea iterar
    # sobre cada elemento del arreglo derecho e izquierdo
    # y cuando ya se haya cumplido una condicion se iterara
    # sobre el siguiente elemento del arreglo que se inserto
    # en el arreglo final(new_numbers)
    left_counter = 0
    rigth_counter = 0
    while True:
        if left_numbers[left_counter] > right_numbers[rigth_counter]:
            print('Se cumplio left')
            new_numbers.append(right_numbers[rigth_counter])
            print(f'new_numbers  luego de left insertion: {new_numbers}')
            rigth_counter += 1
            print(f'Se cumplio left: value now de rigth counter{rigth_counter} ')
        else:    
            new_numbers.append(left_numbers[left_counter])
            print(f'new_numbers   luego de rigth insertion: {new_numbers}')
            left_counter+= 1
            print(f'Se cumplio rigth: value now de left counter{left_counter} ')
        # si se llega a iterar sobre todos los elementos
        # de un arreglo, el ciclo debe terminar
        if rigth_counter == rigth_len:
            break
        elif left_counter == left_len:
            break
    print('Termina  while------------')
    # si existieron elementos de algun subarreglo, derecho o izquierdo
    # que no se llegaron a iterar en el ciclo while
    # se deben insertar al final del arreglo final
    print(f'new_numbers antes de verficar falta de datos: {new_numbers}')
    if left_counter < left_len:
        print('NO termino de insertar los de izquierda')
        new_numbers += left_numbers[left_counter:]
    elif rigth_counter < rigth_len:
        print('No termino de insertar los de DERECHA')
        new_numbers += right_numbers[rigth_counter:]

    print(f'new_numbers: {new_numbers}')
    return new_numbers

result = merge_sorting(numbers)
print(f'result: {result}')