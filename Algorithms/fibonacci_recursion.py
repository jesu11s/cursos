def fibo_recursive(f0,f1,times):
    fn = f0 + f1
    f0 = f1
    f1 = fn
    if times == 1:
        return fn
    result = fibo_recursive(f0,f1,times-1)
    return result
result = fibo_recursive(0,1,34)
print(result)

def fibo_recursive_v2(times):
    if times < 2:
        return times
    return fibo_recursive_v2(times-1) + fibo_recursive_v2(times-2)
result = fibo_recursive_v2(35)
print(result)

def fibo_iterativo(f0,f1,times):
    for i in range(times):
        fn = fn + f0 + f1
        f0 = f1
        f1 = fn
    return fn

result = fibo_recursive(0,1,9)
print(result)