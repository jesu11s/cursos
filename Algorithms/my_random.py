class Random():

    def __init__(self,
                 seed,
                 modulus,
                 multiplier,
                 increment):
        self.seed = seed
        self.modulus = modulus
        self.multiplier = multiplier
        self.increment = increment

    def lcg(self):
        """Linear congruential generator"""
        # print(f"valor de modulus: {self.modulus}")   
        # print(f"valor de multiplier: {self.multiplier}")   
        # print(f"valor de increment: {self.increment}")   
        # print(f"valor de seed: {self.seed}")   
        self.seed = (self.multiplier*self.seed + self.increment) % self.modulus 
        return self.seed 

def lcg_functional(seed,
                   modulus,
                   multiplier,
                   increment):
    """Linear congruential generator"""
    # print(f"valor de modulus: {self.modulus}")   
    # print(f"valor de multiplier: {self.multiplier}")   
    # print(f"valor de increment: {self.increment}")   
    # print(f"valor de seed: {self.seed}")   
    seed = (multiplier*seed + increment) % modulus 
    return seed 

random = Random(8967,333,457,36790)
for i in range(10):
    res = random.lcg()
    print(f"El random: {res}")

print('Otro random por programacion funcional')
res = 8967
modulus = 333
multiplier = 456
increment = 36790
for i in range(10):
    multiplier+=259899
    res = lcg_functional(res,modulus,multiplier,increment)
    print(f"El random: {res}")